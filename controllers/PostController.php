<?php

namespace app\controllers;

use app\models\Post;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends ActiveController
{
    /**
     * @var string the model class name. This property must be set.
     */
    public $modelClass = Post::class;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge([['class' => Cors::class]], parent::behaviors());
    }
}