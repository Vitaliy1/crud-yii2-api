<?php

use yii\db\Migration;

/**
 * Class m190726_103721_post
 */
class m190726_103721_post extends Migration
{

    private const TABLE_POST = 'public.post';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_POST, [
            'id' => $this->primaryKey(),
            'complete' => $this->boolean()->defaultValue(false),
            'title' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_POST);
    }
}
